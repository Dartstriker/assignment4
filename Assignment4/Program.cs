﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Data;

namespace Assignment4
{
	internal partial class Program
	{
		static void Main(string[] args)
		{
			var configuration = new ConfigurationBuilder()
				.AddJsonFile($"appsettings.json");

			var config = configuration.Build();
			var connectionString = config.GetConnectionString("ConnectionString");

			CreateAvitoDbTables(connectionString);

			PopulateTables(connectionString);
			
			ReadTables(connectionString);
		}

		private static void CreateAvitoDbTables(string connectionString)
		{
			using var connection = new NpgsqlConnection(connectionString);
			connection.Open();

			var sqlUsers =
				@"
CREATE SEQUENCE IF NOT EXISTS users_id_seq;

CREATE TABLE IF NOT EXISTS users
(
	id			BIGINT NOT NULL DEFAULT NEXTVAL('users_id_seq'),
	first_name	CHARACTER VARYING(255) NOT NULL,
	last_name	CHARACTER VARYING(255) NOT NULL,
	middle_name CHARACTER VARYING(255),
	email		CHARACTER VARYING(255),

	CONSTRAINT users_pkey PRIMARY KEY (id),
	CONSTRAINT users_email_unique UNIQUE (email)
);

CREATE INDEX IF NOT EXISTS users_last_name_idx ON users (last_name);
CREATE UNIQUE INDEX IF NOT EXISTS users_email_unq_idx ON users (lower(email));
";
			using var usersCmd = new NpgsqlCommand(sqlUsers, connection);
			usersCmd.ExecuteNonQuery();


			var sqlGoods =
				@"
CREATE SEQUENCE IF NOT EXISTS goods_id_seq;

CREATE TABLE IF NOT EXISTS goods
(
	id			BIGINT NOT NULL DEFAULT NEXTVAL('goods_id_seq'),
	name		CHARACTER VARYING(255) NOT NULL,
	description CHARACTER VARYING(255),
	price		DECIMAL NOT NULL,
	seller_id	BIGINT NOT NULL,

	CONSTRAINT goods_pkey PRIMARY KEY (id),
	CONSTRAINT goods_fk_seller_id FOREIGN KEY (seller_id) REFERENCES users(id) ON DELETE CASCADE
);
";
			using var goodsCmd = new NpgsqlCommand(sqlGoods, connection);
			goodsCmd.ExecuteNonQuery();


			var sqlTrades =
				@"
CREATE SEQUENCE IF NOT EXISTS trades_id_seq;

CREATE TABLE IF NOT EXISTS trades
(
	id			BIGINT NOT NULL DEFAULT NEXTVAL('trades_id_seq'),
	goods_id	BIGINT NOT NULL,
	buyer_id	BIGINT NOT NULL,

	CONSTRAINT trades_pkey PRIMARY KEY (id),
	CONSTRAINT trades_fk_buyer_id FOREIGN KEY (buyer_id) REFERENCES users(id) ON DELETE CASCADE
);
";
			using var tradesCmd = new NpgsqlCommand(sqlTrades, connection);
			tradesCmd.ExecuteNonQuery();
		}

		private static void PopulateTables(string connectionString)
		{
			using var connection = new NpgsqlConnection(connectionString); connection.Open();
			var sqlUsers = @"
INSERT INTO users(first_name, last_name, middle_name, email) 
VALUES 
('Иван', 'Иванов', 'Иванович', 'ivan@ya.ru'),
('Петр', 'Петров', 'Петрович', 'pete@ya.ru'),
('Сергей', 'Сергеев', 'Сергеевич', 'sergei@ya.ru'),
('Игорь', 'Игорев', 'Игоревич', 'igor@ya.ru'),
('Егор', 'Егоровов', 'Егорович', 'jeorge@ya.ru');
";
			using var cmdUsers = new NpgsqlCommand(sqlUsers, connection);
			cmdUsers.ExecuteNonQuery();


			var sqlGoods = @"
INSERT INTO goods(name, description, price, seller_id) 
VALUES 
('Видеокарта', 'Хорошая', 16000, 1),
('Процессор', 'Средненький', 8000, 1),
('Лопата', 'Еще дед пользовался', 500, 2),
('Ведро', 'Слегка дырявое', 100, 2),
('Дачные качели', 'На двоих', 6000, 2);
";
			using var cmdGoods = new NpgsqlCommand(sqlGoods, connection);
			cmdGoods.ExecuteNonQuery();


			var sqlTrades = @"
INSERT INTO trades(goods_id, buyer_id) 
VALUES 
(1, 5),
(2, 5),
(3, 4),
(4, 4),
(5, 3);
";
			using var cmdTrades = new NpgsqlCommand(sqlTrades, connection);
			cmdTrades.ExecuteNonQuery();
		}

		private static void ReadTables(string connectionString)
		{
			using var connection = new NpgsqlConnection(connectionString); connection.Open();
			var sql = @"
SELECT first_name, last_name, middle_name, email FROM users;
";
			using var cmd = new NpgsqlCommand(sql, connection);
			var reader = cmd.ExecuteReader();

			while (reader.Read())
			{
				Console.WriteLine($"{reader.GetString(0)} {reader.GetString(1)} {reader.GetString(2)} {reader.GetString(3)}");
			}

			reader.Close();
			Console.WriteLine();

			var sqlGoods = @"
SELECT name, description FROM goods;
";
			using var cmdGoods = new NpgsqlCommand(sqlGoods, connection);
			var readerGoods = cmdGoods.ExecuteReader();

			while (readerGoods.Read())
			{
				Console.WriteLine($"{readerGoods.GetString(0)} {readerGoods.GetString(1)}");
			}

			readerGoods.Close();
		}

		//User Input Example
		private static void PopulateTablesFromInput(string connectionString)
		{
			using var connection = new NpgsqlConnection(connectionString); connection.Open();
			var sql = @"
INSERT INTO users(first_name, last_name, middle_name, email) 
VALUES (:first_name0, :last_name0, :middle_name0, :email0);
";

			using var cmd = new NpgsqlCommand(sql, connection);
			var parameters = cmd.Parameters;
			parameters.Add(new NpgsqlParameter("first_name0", Console.ReadLine())); 
			parameters.Add(new NpgsqlParameter("last_name0", Console.ReadLine())); 
			parameters.Add(new NpgsqlParameter("middle_name0", Console.ReadLine()));
			parameters.Add(new NpgsqlParameter("email0", Console.ReadLine()));

			cmd.ExecuteNonQuery();
		}
	}
}